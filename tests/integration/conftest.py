import aiohttp
import asyncio
import mock
import pytest


@pytest.fixture(scope="session")
def hub(hub):
    hub.pop.sub.add(dyne_name="gate")

    with mock.patch(
        "sys.argv", ["gate", "--port", "5826", "--refs", "init.test*", "--prefix=gate"]
    ):
        hub.pop.config.load(["gate"], cli="gate")

    yield hub


@pytest.mark.asyncio
@pytest.fixture(scope="function")
async def gate(hub):
    gate_server = hub.OPT.gate.server
    coro = hub.gate.init.start(
        gate_server=gate_server,
        host=hub.OPT.gate.host,
        port=hub.OPT.gate.port,
        matcher_plugin=hub.OPT.gate.matcher,
        prefix=hub.OPT.gate.prefix,
        refs=hub.OPT.gate.refs,
    )
    task = hub.pop.Loop.create_task(coro)

    await hub.gate.init.join(gate_server)

    yield

    await hub.gate.init.stop(gate_server)

    await task


@pytest.mark.asyncio
@pytest.fixture(scope="session")
async def requests():
    async with aiohttp.ClientSession() as client:
        yield client
