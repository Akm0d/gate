import pytest


@pytest.mark.asyncio
async def test_basic(hub, gate, requests):
    response = await requests.put(
        "http://127.0.0.1:5826",
        json={"ref": "init.test", "args": [1, 2, 3, 4], "kwargs": {"test": "1"}},
    )
    data = await response.json()
    assert response.status == 200, data
    assert data["kwargs"]["test"] == "1", data
    assert data["args"] == [1, 2, 3, 4], data


@pytest.mark.asyncio
async def test_fail(hub, gate, requests):
    response = await requests.put(
        "http://127.0.0.1:5826", json={"ref": "nonexistent.ref"}
    )
    assert response.status != 200
    data = await response.json()
    assert "error" in data


@pytest.mark.asyncio
async def test_no_ref(hub, gate, requests):
    response = await requests.put("http://127.0.0.1:5826", json={})
    assert response.status != 200
    data = await response.json()
    assert "error" in data
