from typing import List


def sig_match(hub, ref: str, prefix: str, refs: List[str]) -> bool:
    ...
