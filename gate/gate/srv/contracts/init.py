from typing import List


async def sig_start(
    hub, host: str, port: int, matcher_plugin: str, prefix: str, refs: List[str]
):
    ...
