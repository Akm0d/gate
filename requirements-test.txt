aiohttp
asynctest>=0.13.0
mock>=4.0.2
pytest>=5.4.1
pytest-asyncio>=0.10.0
pytest-pop>=5
pytest-subtests
pyyaml
